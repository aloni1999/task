## Polarsec task

### Part 1 - Hands on

We need to build the back-end for a super cool new app called **Lets Run!**

The back-end has a web API with the following endpoints:

* `/signup`
* `/update`
* `/mystats`

The _Lets Run!_ developers wanted to be unique, in that their app does not require using a password (they totally hate remembering those) and doesn't use social sign in methods such as Google and Facebook (they totally don't trust them).

In order to achieve this, whenever a user signs up via the `/signup` endpoint, the back-end creates a **private/public key pair** and sends the private key back to the app as a response. The app (which isn't part of the task) saves the private key and uses it in order to authenticate requests it sends to the rest of the web API's endpoints.

It is required that the project use **docker**. This means that all of the components be in a single or multiple docker containers and running the command `docker-compose up` is all that is needed in order for everything to work (web server, DB, etc.).

Each user that signs up to the application should have the following attributes

name - The users full name  
age - The users age  
city - Where the user is located  
total_distance_run - The total distance the user has ran, when the user has just signed up, this should be 0.

Lets go over each endpoint in order to understand what they are expected to do:

### `/signup`
Description: Signs the user up to the application

Receives the following JSON in the body of an HTTP post
```
{
  "name": "John Doe",
  "age": 31,
  "city": "New York"
}
```

Returns the follwing JSON
```
{
  "privateKey": <the_private_key>,  
}
```

The information needs to be stored in a DB, as it will be needed in the future. In addition, a public/private key pair needs to be created, with the private key being returned in the response.  
In order to authenticate the users' future requests, the public key should also be stored and associated with the user.

The encryption algorithim used when creating the keys needs to be **RSA 2048**

Note that it **is** possible for there to be more than one user with the same first and last name!

### `/update`
Description: Updates the users total running distance

Receives the following JSON in the body of an HTTP post

```
{
  "request": "xxxxx.yyyyy"
}
```

(the exact number of x's and y's can change)

`xxxxx` is a *base64* encoded JSON 
```
{
  "name": "John Doe",
  "distance": 10
}
```
`yyyyy` is the signature of `xxxxx` that was created using the private key returned in the `signup` endpoint. You can assume that the signature `yyyyy` is in base64 as well.  
After the signature is verified, the users' running data is updated. So in this case if John's current `total_distance_run` is 30, it should be updated to 40.

Returns the follwing JSON
```
{
  "totalDistanceRun": <total_distance_run (or -1 if fail)>,  
}
```


### `/mystats`
Description: Returns the users' ranking

Receives the following JSON in the body of an HTTP post

```
{
  "request": "xxxxx.yyyyy"
}
```
(the exact number of x's and y's can change)

`xxxxx` is a *base64* encoded JSON 
```
{
  "name": "John Doe",
  "type": "<stat_option>"
}
```

Returns the ranking of the in the following JSON . Users are ranked based on how much distance they have run.
```
{
  "ranking": <ranking (or -1 if fail)>,  
}
```

<stat_option> can be one of:  
"city" - The ranking of user in his own city. In this example, John Doe lives in New York, so if out of all of the people living in New York he is 4th place, the number 4 will be returned.  
"age" - The ranking of the user in his own age group. In this example, John Doe is 31 years old, so if out of all of the users of the application (regardless of where they live) that are aged 31 he is in 2nd place, the number 2 will be returned.  
"overall" - The ranking of the user among all users of the application. In this example, if out of all users of the application (regardless of where they live and their age) John is in 7th place, the number 7 will be returned.

The ranking is very basic and is determined solely by the total distance run.

`yyyyy` is the signature of `xxxxx` that was made using the private key returned in the `signup` endpoint. After the signature is verified, the endpoint returns the users ranking. 

### Part 2 - Theoretical 

_Lets Run!_ has become super popular which while obviously goods news, has surfaced a new problem.

Apparently, people really like running at 6AM, which causes spikes in the requests sent to the `/update` endpoint, overloading the network and causing slowdowns and unresponsiveness in the application. 

How would you solve this issue? Please provide 2 suggestions.
